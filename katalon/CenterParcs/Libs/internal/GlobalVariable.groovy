package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object spaFlexibleDays
     
    /**
     * <p></p>
     */
    public static Object spaVillageValue
     
    /**
     * <p>Profile AquaSanaSearchParameters : Today, 3 Months, 6 Months</p>
     */
    public static Object spaDateValue
     
    /**
     * <p></p>
     */
    public static Object spaDateRequired
     
    /**
     * <p></p>
     */
    public static Object spaVillageRequired
     
    /**
     * <p></p>
     */
    public static Object spaFlexibleDaysRequired
     
    /**
     * <p></p>
     */
    public static Object luxuryFlexibleDays
     
    /**
     * <p></p>
     */
    public static Object luxuryVillageValue
     
    /**
     * <p>Profile AquaSanaSearchParameters : Today, 3 Months, 6 Months</p>
     */
    public static Object luxuryDateValue
     
    /**
     * <p></p>
     */
    public static Object luxuryDateRequired
     
    /**
     * <p></p>
     */
    public static Object luxuryVillageRequired
     
    /**
     * <p></p>
     */
    public static Object luxuryFlexibleDayRequired
     
    /**
     * <p></p>
     */
    public static Object alreadyBookedTreatment
     
    /**
     * <p></p>
     */
    public static Object alreadyBookedCP
     
    /**
     * <p></p>
     */
    public static Object dateValue
     
    /**
     * <p></p>
     */
    public static Object paySetAmount
     
    /**
     * <p>Profile CenterParcsSearchParameters : WF = Whinfell Forest, SF = Sherwood Forest, EF = Elveden Forest WO = Woburn Forest, LF = Longleat Forest, IR =  Longford Forest</p>
     */
    public static Object village
     
    /**
     * <p>Profile CenterParcsSearchParameters : 1, 2 or 3</p>
     */
    public static Object lodges
     
    /**
     * <p>Profile CenterParcsSearchParameters : Standard is 2, change to greater than 2 if more are required or less if less are required</p>
     */
    public static Object adults
     
    /**
     * <p></p>
     */
    public static Object children2_5
     
    /**
     * <p></p>
     */
    public static Object children6_16
     
    /**
     * <p></p>
     */
    public static Object dogs
     
    /**
     * <p>Profile guestProfile : Will be generated as part of SettingUserDetails test case</p>
     */
    public static Object guestEmailAddress
     
    /**
     * <p>Profile guestProfile : Will be generated as part of SettingUserDetails test case</p>
     */
    public static Object guestFirstName
     
    /**
     * <p>Profile guestProfile : Will be generated as part of SettingUserDetails test case</p>
     */
    public static Object guestLastName
     
    /**
     * <p>Profile userProfile : Will be generated as part of SettingUserDetails test case</p>
     */
    public static Object emailAddress
     
    /**
     * <p>Profile userProfile : Will be generated as part of SettingUserDetails test case</p>
     */
    public static Object firstName
     
    /**
     * <p>Profile userProfile : Will be generated as part of SettingUserDetails test case</p>
     */
    public static Object lastName
     
    /**
     * <p></p>
     */
    public static Object emailComms
     
    /**
     * <p></p>
     */
    public static Object postComms
     
    /**
     * <p></p>
     */
    public static Object smsComms
     
    /**
     * <p></p>
     */
    public static Object currentPassword
     
    /**
     * <p></p>
     */
    public static Object newPassword
     
    /**
     * <p></p>
     */
    public static Object getGuestEmail
     
    /**
     * <p></p>
     */
    public static Object uid
     
    /**
     * <p></p>
     */
    public static Object sessionId
     
    /**
     * <p></p>
     */
    public static Object accommodationCode
     
    /**
     * <p></p>
     */
    public static Object villageCode
     
    /**
     * <p></p>
     */
    public static Object arrivalDate
     
    /**
     * <p></p>
     */
    public static Object departureDate
     
    /**
     * <p></p>
     */
    public static Object adultsNo
     
    /**
     * <p></p>
     */
    public static Object childNo
     
    /**
     * <p></p>
     */
    public static Object dogsNo
     
    /**
     * <p></p>
     */
    public static Object bookingRef
     
    /**
     * <p></p>
     */
    public static Object totalPrice
     
    /**
     * <p></p>
     */
    public static Object itinSeqNumber
     
    /**
     * <p></p>
     */
    public static Object paymentGatewayURL
     
    /**
     * <p></p>
     */
    public static Object villaCategory
     
    /**
     * <p></p>
     */
    public static Object villaType
     
    /**
     * <p></p>
     */
    public static Object villaNumber
     
    /**
     * <p></p>
     */
    public static Object guestUid
     
    /**
     * <p>Profile webServices : 2 Weeks, 3 Months, 6 Months &amp; 12 Months</p>
     */
    public static Object bookingDates
     

    static {
        def allVariables = [:]        
        allVariables.put('default', [:])
        allVariables.put('AquaSanaSearchParameters', allVariables['default'] + ['spaFlexibleDays' : 'null', 'spaVillageValue' : 'SF', 'spaDateValue' : 'Today', 'spaDateRequired' : true, 'spaVillageRequired' : true, 'spaFlexibleDaysRequired' : false, 'luxuryFlexibleDays' : 'null', 'luxuryVillageValue' : 'EF', 'luxuryDateValue' : 'Today', 'luxuryDateRequired' : true, 'luxuryVillageRequired' : true, 'luxuryFlexibleDayRequired' : false, 'alreadyBookedTreatment' : true, 'alreadyBookedCP' : true, 'dateValue' : ''])
        allVariables.put('CenterParcsParameters', allVariables['default'] + ['paySetAmount' : false])
        allVariables.put('CenterParcsSearchParameters', allVariables['default'] + ['village' : 'WF', 'lodges' : '1', 'adults' : 2, 'children2_5' : 0, 'children6_16' : 0, 'dogs' : 0])
        allVariables.put('guestProfile', allVariables['default'] + ['guestEmailAddress' : 'Test', 'guestFirstName' : 'Test', 'guestLastName' : 'Test'])
        allVariables.put('userProfile', allVariables['default'] + ['emailAddress' : 'Test', 'firstName' : 'Test', 'lastName' : 'Test', 'emailComms' : true, 'postComms' : true, 'smsComms' : true, 'currentPassword' : '', 'newPassword' : ''])
        allVariables.put('webServices', allVariables['default'] + ['getGuestEmail' : '', 'uid' : '', 'sessionId' : '', 'accommodationCode' : '', 'villageCode' : '', 'arrivalDate' : '', 'departureDate' : '', 'adultsNo' : '', 'childNo' : '', 'dogsNo' : '', 'bookingRef' : '', 'totalPrice' : '', 'itinSeqNumber' : '', 'paymentGatewayURL' : '', 'villaCategory' : '', 'villaType' : '', 'villaNumber' : '', 'guestUid' : '', 'bookingDates' : ''])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        spaFlexibleDays = selectedVariables['spaFlexibleDays']
        spaVillageValue = selectedVariables['spaVillageValue']
        spaDateValue = selectedVariables['spaDateValue']
        spaDateRequired = selectedVariables['spaDateRequired']
        spaVillageRequired = selectedVariables['spaVillageRequired']
        spaFlexibleDaysRequired = selectedVariables['spaFlexibleDaysRequired']
        luxuryFlexibleDays = selectedVariables['luxuryFlexibleDays']
        luxuryVillageValue = selectedVariables['luxuryVillageValue']
        luxuryDateValue = selectedVariables['luxuryDateValue']
        luxuryDateRequired = selectedVariables['luxuryDateRequired']
        luxuryVillageRequired = selectedVariables['luxuryVillageRequired']
        luxuryFlexibleDayRequired = selectedVariables['luxuryFlexibleDayRequired']
        alreadyBookedTreatment = selectedVariables['alreadyBookedTreatment']
        alreadyBookedCP = selectedVariables['alreadyBookedCP']
        dateValue = selectedVariables['dateValue']
        paySetAmount = selectedVariables['paySetAmount']
        village = selectedVariables['village']
        lodges = selectedVariables['lodges']
        adults = selectedVariables['adults']
        children2_5 = selectedVariables['children2_5']
        children6_16 = selectedVariables['children6_16']
        dogs = selectedVariables['dogs']
        guestEmailAddress = selectedVariables['guestEmailAddress']
        guestFirstName = selectedVariables['guestFirstName']
        guestLastName = selectedVariables['guestLastName']
        emailAddress = selectedVariables['emailAddress']
        firstName = selectedVariables['firstName']
        lastName = selectedVariables['lastName']
        emailComms = selectedVariables['emailComms']
        postComms = selectedVariables['postComms']
        smsComms = selectedVariables['smsComms']
        currentPassword = selectedVariables['currentPassword']
        newPassword = selectedVariables['newPassword']
        getGuestEmail = selectedVariables['getGuestEmail']
        uid = selectedVariables['uid']
        sessionId = selectedVariables['sessionId']
        accommodationCode = selectedVariables['accommodationCode']
        villageCode = selectedVariables['villageCode']
        arrivalDate = selectedVariables['arrivalDate']
        departureDate = selectedVariables['departureDate']
        adultsNo = selectedVariables['adultsNo']
        childNo = selectedVariables['childNo']
        dogsNo = selectedVariables['dogsNo']
        bookingRef = selectedVariables['bookingRef']
        totalPrice = selectedVariables['totalPrice']
        itinSeqNumber = selectedVariables['itinSeqNumber']
        paymentGatewayURL = selectedVariables['paymentGatewayURL']
        villaCategory = selectedVariables['villaCategory']
        villaType = selectedVariables['villaType']
        villaNumber = selectedVariables['villaNumber']
        guestUid = selectedVariables['guestUid']
        bookingDates = selectedVariables['bookingDates']
        
    }
}
