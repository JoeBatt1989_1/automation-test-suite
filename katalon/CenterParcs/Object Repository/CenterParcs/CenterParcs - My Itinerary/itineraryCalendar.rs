<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>itineraryCalendar</name>
   <tag></tag>
   <elementGuidId>db9cffd8-17b1-433b-bcfb-fbd010d7439b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'cp-list-slider--is-active']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.itinerary-grid-and-filter</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>cp-list-slider--is-active</value>
   </webElementProperties>
</WebElementEntity>
