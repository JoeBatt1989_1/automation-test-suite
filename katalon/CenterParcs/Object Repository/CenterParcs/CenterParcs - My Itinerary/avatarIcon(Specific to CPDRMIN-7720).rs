<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>avatarIcon(Specific to CPDRMIN-7720)</name>
   <tag></tag>
   <elementGuidId>2431e24c-8bca-4663-b70f-26fd0bb96f68</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[4]/div/div[2]/div/div[3]/section/div/div/div[2]/main/div[2]/div[1]/div/div/div/div/div[8]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>/html/body/div[4]/div/div[2]/div/div[3]/section/div/div/div[2]/main/div[2]/div[1]/div/div/div/div/div[8]/a</value>
   </webElementXpaths>
</WebElementEntity>
