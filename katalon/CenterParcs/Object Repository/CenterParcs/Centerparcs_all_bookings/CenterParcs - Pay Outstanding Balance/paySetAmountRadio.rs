<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>paySetAmountRadio</name>
   <tag></tag>
   <elementGuidId>61f0fb2c-bc1a-4e02-afc3-e62144223ade</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'paymentOption']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>fieldset.radio-switch-group label:nth-child(2) input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>freeAmount</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>paymentOption</value>
   </webElementProperties>
</WebElementEntity>
