<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>viewBookingOverview(Specific to test CPDRMIN-4652)</name>
   <tag></tag>
   <elementGuidId>b1962db2-8e2d-42b7-b25b-a77aa59df5fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.root.responsivegrid > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > main > div > div:nth-child(1) > div > div.allbookings.parbase.aem-GridColumn.aem-GridColumn--default--12 > div > div:nth-child(4) > article:nth-child(15) > div.item-block__card__wrapper > div > span.item-block__card__btn.js-booking-detail > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value></value>
   </webElementXpaths>
</WebElementEntity>
