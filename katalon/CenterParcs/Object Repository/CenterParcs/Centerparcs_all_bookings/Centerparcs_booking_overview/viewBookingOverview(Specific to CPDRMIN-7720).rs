<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>viewBookingOverview(Specific to CPDRMIN-7720)</name>
   <tag></tag>
   <elementGuidId>58e85755-55eb-4773-a3c3-b81c22a3dab9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>body > div.root.responsivegrid > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > main > div > div:nth-child(1) > div > div.allbookings.parbase.aem-GridColumn.aem-GridColumn--default--12 > div > div:nth-child(4) > article > div.item-block__card__wrapper > div > span.item-block__card__btn.js-booking-detail > a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
