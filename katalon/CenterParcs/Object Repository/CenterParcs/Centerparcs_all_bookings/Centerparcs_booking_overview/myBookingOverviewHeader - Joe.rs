<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>myBookingOverviewHeader - Joe</name>
   <tag></tag>
   <elementGuidId>566dac3d-d839-47b1-a994-3ca7eea4db34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html/body/div[4]/div/div[2]/div/main/div/div[1]/div/div[1]/h1</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'My booking overview') or contains(., 'My booking overview'))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>My booking overview</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>/html/body/div[4]/div/div[2]/div/main/div/div[1]/div/div[1]/h1</value>
   </webElementXpaths>
</WebElementEntity>
