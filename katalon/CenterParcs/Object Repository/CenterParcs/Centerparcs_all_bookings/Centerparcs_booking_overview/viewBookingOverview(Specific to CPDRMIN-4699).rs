<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>viewBookingOverview(Specific to CPDRMIN-4699)</name>
   <tag></tag>
   <elementGuidId>328a0f10-1a67-44c4-bd61-79f85cb64748</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.root.responsivegrid > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > main > div > div:nth-child(1) > div > div.allbookings.parbase.aem-GridColumn.aem-GridColumn--default--12 > div > div:nth-child(4) > article > div.item-block__card__wrapper > div > span.item-block__card__btn.js-booking-detail > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
