<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>continueBtn</name>
   <tag></tag>
   <elementGuidId>36d09444-e125-41c0-bcef-cef06db5120c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.details__submit div:nth-child(2) button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>.details__submit div:nth-child(2) button</value>
   </webElementProperties>
</WebElementEntity>
