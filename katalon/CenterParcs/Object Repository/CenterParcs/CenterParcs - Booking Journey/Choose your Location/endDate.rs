<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>endDate</name>
   <tag></tag>
   <elementGuidId>fa5222b6-bc3d-4425-9d3a-29fb5fc26deb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > div.pika-single.start-date.is-bound.is-hidden > div:nth-child(3) > table > tbody > tr:nth-child(2) > td.is-endrange.is-standard-check-in > button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value></value>
   </webElementXpaths>
</WebElementEntity>
