<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>signInBtn</name>
   <tag></tag>
   <elementGuidId>4d3a5449-2876-4fb6-ae28-c12f3f9350c3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>body > div.root.responsivegrid > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > main > section > aside > div > div > div > div.booking-block__cta > a.btn.btn--action.btn--large.signed-out.show</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
