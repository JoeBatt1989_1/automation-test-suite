<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>guestDetailsCard - Joe</name>
   <tag></tag>
   <elementGuidId>e721b0a9-2237-4703-8e5c-9d6387e6341f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>.icon-card__link .js-edit-guest-link</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>.icon-card__link .js-edit-guest-link</value>
   </webElementProperties>
</WebElementEntity>
