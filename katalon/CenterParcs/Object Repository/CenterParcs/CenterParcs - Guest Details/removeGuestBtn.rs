<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>removeGuestBtn</name>
   <tag></tag>
   <elementGuidId>37b495bf-8a13-4c77-ac31-d16e2d13112d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn--half.btn--action.js-remove-guest-form-submit</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(contains(text(), 'Remove Guest') or contains(., 'Remove Guest')) and @class = 'js-remove-guest-form-submit']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Remove Guest</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>js-remove-guest-form-submit</value>
   </webElementProperties>
</WebElementEntity>
