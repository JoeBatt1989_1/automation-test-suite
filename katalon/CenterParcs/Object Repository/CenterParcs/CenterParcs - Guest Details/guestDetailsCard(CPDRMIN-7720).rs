<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>guestDetailsCard(CPDRMIN-7720)</name>
   <tag></tag>
   <elementGuidId>9d2baf58-7b29-4e80-8f94-f2d78495c4f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>body > div.root.responsivegrid > div > div.responsivegrid.aem-GridColumn.aem-GridColumn--default--12 > div > main > div > div.my-booking-content > div > div > div:nth-child(2) > div > div > div > div:nth-child(1) > div > a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
