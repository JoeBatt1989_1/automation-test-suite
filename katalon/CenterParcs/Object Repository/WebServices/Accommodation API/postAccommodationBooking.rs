<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>postAccommodationBooking</name>
   <tag></tag>
   <elementGuidId>41c38323-8a41-4c89-90e8-b0aa6bf06d37</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://10.65.140.4:9444/service-accommodation-v1.16.1/api/accommodations/villagecode/${villageCode}/accommodationCode/${accommodationCode}?sessionid=${sessionId}&amp;sequenceNo=1&amp;adults=${adultsNo}&amp;arrivalDate=${arrivalDate}&amp;duration=4</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.sessionId</defaultValue>
      <description></description>
      <id>917b4e02-3247-4911-b0ca-fb0fb0151248</id>
      <masked>false</masked>
      <name>sessionId</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.accommodationCode</defaultValue>
      <description></description>
      <id>ed8ebaaa-f07c-4774-8706-892ca79494d3</id>
      <masked>false</masked>
      <name>accommodationCode</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.villageCode</defaultValue>
      <description></description>
      <id>8e088de6-a360-462b-929f-0aa7a0f035c3</id>
      <masked>false</masked>
      <name>villageCode</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.adultsNo</defaultValue>
      <description></description>
      <id>fa6458ca-bc70-443d-a27c-90f4a4379dd8</id>
      <masked>false</masked>
      <name>adultsNo</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.arrivalDate</defaultValue>
      <description></description>
      <id>3081c30e-f93e-4376-a119-53d9993a4a30</id>
      <masked>false</masked>
      <name>arrivalDate</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
