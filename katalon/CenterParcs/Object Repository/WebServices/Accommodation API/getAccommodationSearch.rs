<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>getAccommodationSearch</name>
   <tag></tag>
   <elementGuidId>5f9b8dab-3480-4abd-87e8-d472f3952db8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://10.65.140.4:9444/service-accommodation-v1.16.1/api/accommodations/villagecode/${villageCode}?sessionid=${sessionId}&amp;sequenceNo=0&amp;adults=${adultsNo}&amp;minBedrooms=1&amp;arrivalDateFrom=${arrivalDate}&amp;arrivalDateTo=${departureDate}&amp;duration=4</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.sessionId</defaultValue>
      <description></description>
      <id>6e4a3720-6a55-445e-a72a-fd6e252472a6</id>
      <masked>false</masked>
      <name>sessionId</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.null</defaultValue>
      <description></description>
      <id>a2af060d-f0d8-4ef2-8ccf-aba99b129f61</id>
      <masked>false</masked>
      <name>adultsNo</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.arrivalDate</defaultValue>
      <description></description>
      <id>0789885e-c8b5-4d74-b7f0-7fcdc865a3cd</id>
      <masked>false</masked>
      <name>arrivalDate</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.departureDate</defaultValue>
      <description></description>
      <id>729c4b72-a1ee-45c4-a503-1f4b0c47819b</id>
      <masked>false</masked>
      <name>departureDate</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.villageCode</defaultValue>
      <description></description>
      <id>dce9fc5d-a585-4d9f-a760-0c589a31cdc3</id>
      <masked>false</masked>
      <name>villageCode</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
