<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>postGuestsAddNoComms</name>
   <tag></tag>
   <elementGuidId>a8071505-6790-4689-b525-9a4dec3ff29e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;email\&quot;: \&quot;cpsit.tester1+${emailAddress}@gmail.com\&quot;,\n  \&quot;title\&quot;: \&quot;Mr\&quot;,\n  \&quot;firstName\&quot;: \&quot;John\&quot;,\n  \&quot;lastName\&quot;: \&quot;Smith\&quot;,\n  \&quot;gender\&quot;: \&quot;m\&quot;,\n  \&quot;birthDay\&quot;: \&quot;01\&quot;,\n  \&quot;birthMonth\&quot;: \&quot;01\&quot;,\n  \&quot;birthYear\&quot;: \&quot;1980\&quot;,\n  \&quot;password\&quot;: \&quot;Password123\&quot;,\n  \&quot;phones\&quot;: [\n    {\n      \&quot;number\&quot;: \&quot;07777777777\&quot;,\n      \&quot;countryCode\&quot;: \&quot;+44\&quot;,\n      \&quot;type\&quot;: \&quot;Mobile\&quot;\n    }\n  ],\n  \&quot;address\&quot;: {\n    \&quot;address1\&quot;: \&quot;Center Parcs Ltd\&quot;,\n    \&quot;address2\&quot;: \&quot;1 Edison Rise\&quot;,\n    \&quot;city\&quot;: \&quot;Nottingham\&quot;,\n    \&quot;country\&quot;: \&quot;UK\&quot;,\n    \&quot;county\&quot;: \&quot;NEWARK\&quot;,\n    \&quot;postcode\&quot;: \&quot;NG22 9DP\&quot;\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://10.65.140.4:9444/service-guest-v1.21.1/api/guests/add</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.emailAddress</defaultValue>
      <description></description>
      <id>d948f64c-74c4-4e44-907b-d28560ea6192</id>
      <masked>false</masked>
      <name>emailAddress</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.firstName</defaultValue>
      <description></description>
      <id>f04faad6-513d-4ba0-8730-a95688b6078e</id>
      <masked>false</masked>
      <name>firstName</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.lastName</defaultValue>
      <description></description>
      <id>7d6ca974-d627-4055-a4c0-83a0a792f66c</id>
      <masked>false</masked>
      <name>lastName</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
