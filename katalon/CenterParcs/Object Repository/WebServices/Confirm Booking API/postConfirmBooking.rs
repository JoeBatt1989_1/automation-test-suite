<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>postConfirmBooking</name>
   <tag></tag>
   <elementGuidId>d093e92b-3eaf-4c04-a86a-7243963a2396</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;uid\&quot;: \&quot;${uid}\&quot;,\n  \&quot;title\&quot;: \&quot;Mr\&quot;,\n  \&quot;firstName\&quot;: \&quot;John\&quot;,\n  \&quot;lastName\&quot;: \&quot;Smith\&quot;,\n  \&quot;email\&quot;: \&quot;cpsit.tester1+${emailAddress}@gmail.com\&quot;,\n  \&quot;gender\&quot;: \&quot;m\&quot;,\n  \&quot;birthDay\&quot;: 1,\n  \&quot;birthMonth\&quot;: 1,\n  \&quot;birthYear\&quot;: 1980,\n  \&quot;address\&quot;: {\n    \&quot;address1\&quot;: \&quot;Center Parcs Ltd\&quot;,\n    \&quot;address2\&quot;: \&quot;1 Edison Rise\&quot;,\n    \&quot;city\&quot;: \&quot;Nottingham\&quot;,\n    \&quot;postcode\&quot;: \&quot;NG22 9DP\&quot;,\n    \&quot;country\&quot;: \&quot;UK\&quot;,\n    \&quot;ccounty\&quot;: \&quot;NEWARK\&quot;\n  },\n  \&quot;phones\&quot;: [\n    {\n      \&quot;type\&quot;: \&quot;Mobile\&quot;,\n      \&quot;countryCode\&quot;: \&quot;+44\&quot;,\n      \&quot;number\&quot;: \&quot;07777777777\&quot;\n    }\n  ],\n  \&quot;cpPreferences\&quot;: {\n    \&quot;subscribe_cp_email\&quot;: false,\n    \&quot;subscribe_cp_directmail\&quot;: false,\n    \&quot;subscribe_cp_sms\&quot;: false,\n    \&quot;subscribe_cp_phone\&quot;: false\n  },\n  \&quot;vehicleDetailsList\&quot;: [\n    {\n      \&quot;make\&quot;: \&quot;Volvo\&quot;,\n      \&quot;model\&quot;: \&quot;V40\&quot;,\n      \&quot;registration\&quot;: \&quot;YY18YYY\&quot;,\n      \&quot;type\&quot;: \&quot;Car\&quot;\n    }\n  ],\n  \&quot;participatingGuests\&quot;: [\n  ],\n  \&quot;canAmendBooking\&quot;: false,\n  \&quot;villa\&quot;: {\n    \&quot;villaNum\&quot;: \&quot;${villaNum}\&quot;,\n    \&quot;villaTyp\&quot;: \&quot;${villaType}\&quot;,\n    \&quot;villaCat\&quot;: \&quot;${villaCat}\&quot;\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://10.65.140.4:9444/service-accommodation-v1.16.1/api/accommodations/bookings/confirm?sessionid=${sessionId}&amp;sequenceNo=3</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.emailAddress</defaultValue>
      <description></description>
      <id>0f9e7145-d6cc-42fb-a6db-0906579141e2</id>
      <masked>false</masked>
      <name>emailAddress</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.uid</defaultValue>
      <description></description>
      <id>56e9345a-2bc8-4d7e-88ed-4dc59d66672a</id>
      <masked>false</masked>
      <name>uid</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.villaNumber</defaultValue>
      <description></description>
      <id>a10df3cd-b1f3-4f78-8214-b1e749c627d8</id>
      <masked>false</masked>
      <name>villaNum</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.villaCategory</defaultValue>
      <description></description>
      <id>790d041b-d8de-4385-866b-b98703932890</id>
      <masked>false</masked>
      <name>villaCat</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.villaType</defaultValue>
      <description></description>
      <id>aabd635c-7a06-4e48-af37-1e2ac259b8c0</id>
      <masked>false</masked>
      <name>villaType</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.sessionId</defaultValue>
      <description></description>
      <id>69fdc976-6445-40b5-9246-237e903f34b4</id>
      <masked>false</masked>
      <name>sessionId</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
