<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>postPaymentGateway</name>
   <tag></tag>
   <elementGuidId>1140d6fd-c19c-462d-8929-fd7726169c23</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;uid\&quot;: \&quot;${uid}\&quot;,\n  \&quot;title\&quot;: \&quot;Mr\&quot;,\n  \&quot;firstName\&quot;: \&quot;John\&quot;,\n  \&quot;lastName\&quot;: \&quot;Smith\&quot;,\n  \&quot;email\&quot;: \&quot;cpsit.tester1+${emailAddress}@gmail.com\&quot;,\n  \&quot;gender\&quot;: \&quot;m\&quot;,\n  \&quot;birthDay\&quot;: 1,\n  \&quot;birthMonth\&quot;: 1,\n  \&quot;birthYear\&quot;: 1980,\n  \&quot;address\&quot;: {\n    \&quot;address1\&quot;: \&quot;Center Parcs Ltd\&quot;,\n    \&quot;address2\&quot;: \&quot;1 Edison Rise\&quot;,\n    \&quot;city\&quot;: \&quot;Nottingham\&quot;,\n    \&quot;postcode\&quot;: \&quot;NG22 9DP\&quot;,\n    \&quot;country\&quot;: \&quot;UK\&quot;,\n    \&quot;ccounty\&quot;: \&quot;NEWARK\&quot;\n  },\n  \&quot;phones\&quot;: [\n    {\n      \&quot;type\&quot;: \&quot;Mobile\&quot;,\n      \&quot;countryCode\&quot;: \&quot;+44\&quot;,\n      \&quot;number\&quot;: \&quot;07777777777\&quot;\n    }\n  ],\n  \&quot;cpPreferences\&quot;: {\n    \&quot;subscribe_cp_email\&quot;: false,\n    \&quot;subscribe_cp_directmail\&quot;: false,\n    \&quot;subscribe_cp_sms\&quot;: false,\n    \&quot;subscribe_cp_phone\&quot;: false\n  }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://10.65.140.4:9444/service-accommodation-v1.16.1/api/accommodations/payments/gateway?sessionid=${sessionId}&amp;sequenceNo=2&amp;minPayment=1000&amp;maxPayment=1000&amp;landingURL=https%3A%2F%2Fwww.google.co.uk%2F</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.sessionId</defaultValue>
      <description></description>
      <id>b93f8da3-c520-4616-b7a2-b858903d2a33</id>
      <masked>false</masked>
      <name>sessionId</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.emailAddress</defaultValue>
      <description></description>
      <id>33e165cd-b9a4-4840-8516-69ffbcc3db5e</id>
      <masked>false</masked>
      <name>emailAddress</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.uid</defaultValue>
      <description></description>
      <id>089554cb-1bb0-42c6-8bc0-8e5565d5093f</id>
      <masked>false</masked>
      <name>uid</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
