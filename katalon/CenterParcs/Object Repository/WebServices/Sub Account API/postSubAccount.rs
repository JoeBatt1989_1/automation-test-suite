<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>postSubAccount</name>
   <tag></tag>
   <elementGuidId>8d200709-8575-4e8d-958e-7951d66e8800</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n  \&quot;id\&quot;: 0,\n  \&quot;title\&quot;: \&quot;Mr\&quot;,\n  \&quot;email\&quot;: \&quot;cpsit.tester1+${guestEmail}@gmail.com\&quot;,\n  \&quot;firstName\&quot;: \&quot;John\&quot;,\n  \&quot;lastName\&quot;: \&quot;Bell\&quot;,\n  \&quot;gender\&quot;: \&quot;m\&quot;,\n  \&quot;birthDay\&quot;: \&quot;01\&quot;,\n  \&quot;birthMonth\&quot;: \&quot;01\&quot;,\n  \&quot;birthYear\&quot;: \&quot;1989\&quot;,\n  \&quot;uid\&quot;: \&quot;${guestUid}\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://10.65.140.4:9444/service-guest-v1.21.1/api/guests/id/${uid}/addSubAccount?Auth-Token=AuthToken</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.uid</defaultValue>
      <description></description>
      <id>bfc19b4e-91c8-4472-8690-514decbbb314</id>
      <masked>false</masked>
      <name>uid</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.guestUid</defaultValue>
      <description></description>
      <id>536b55cb-e8e0-48d9-ac18-e581ce8c8ebd</id>
      <masked>false</masked>
      <name>guestUid</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.guestEmailAddress</defaultValue>
      <description></description>
      <id>2c62250c-bf1b-4e36-a1fd-fb1ca9d337f2</id>
      <masked>false</masked>
      <name>guestEmail</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
