<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>cookiePolicyAcceptBtn</name>
   <tag></tag>
   <elementGuidId>c7adbeb5-2c10-4bcf-90de-8720a585ad1b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > footer > div.cookie-notice-container > a.cn-set-cookie.button</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html/body/footer/div[2]/a[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value></value>
   </webElementXpaths>
</WebElementEntity>
