<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>magnetOpen</name>
   <tag></tag>
   <elementGuidId>1543b25b-d7e4-4a9a-9300-452b5acf5164</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.btn-magnet-close .js-close-booking-magnet .is-visible-sm .is-closed</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
