<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>AquaSana</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>62f2e8e4-4504-41cc-bed0-849ba7ca192a</testSuiteGuid>
   <testCaseLink>
      <guid>cbe2e258-339b-4f9f-9117-e6132d166738</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4525 - Check_Header_Links</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42192cd2-922f-4cb0-b79c-21dcc016bd72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4526 - Search_Results_Page_And_Refine_Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>501fca67-4e31-4fac-9895-96d192d33f56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4530 - Booking_Magnet_Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce053dc0-04ad-4ec3-8980-58f3dc4bbe50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4531 - Booking_Magnet_Page_Navigation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e6b1034-c6dd-4b43-b01d-29e7e46b5529</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4532 - User_Changes_Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2954c30e-9f0d-48dd-9d43-2e45ebafee58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4542 - Registration_Via_Email_Using_Top_Right_Link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0483923-e6f6-4b46-b9d9-0df09c699d44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4545 - Standalone_Sign_In</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9409646d-fdb0-42a1-b1c1-699d4789447e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4546 - Newsletter_Sign_Up</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb16ee85-8978-4245-b2c0-e776947fd362</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4547 - Booking_Create_An_Account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95f70923-f565-4449-b035-1b7a2944343a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4551 - The_Aquasana_Experience_Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d367a3fe-1cc3-42b9-a9bf-3a5baa751d1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AquaSana/CPDRMIN-4538 - My_Information_Page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
