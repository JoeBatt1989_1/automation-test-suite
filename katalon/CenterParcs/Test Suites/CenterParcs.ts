<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CenterParcs</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-10-08T16:11:57</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e0c01beb-85d1-407e-8338-ffede250c73e</testSuiteGuid>
   <testCaseLink>
      <guid>c42ace98-e356-4948-8950-e25b3e708773</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-7698 - Booking_Outside_Of_6_Month_Window</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1fba87a6-7e37-4bb1-a1f9-41f319efc81c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-4702 - Registration_Tickbox</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8e82901e-511e-48fd-a86d-8d518c3085a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-4720 - User_Gets_Locked_Out_Of_Account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>939aa481-c88f-45a0-9848-d9cc056c53ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-4704 - Registration_Correct_Details</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cbd15229-928a-44f0-8410-f092023dd1a3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>57b2a6db-f414-4901-826f-b7fc293514fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-4734 - Account_Registration_And_Select_Email_Preferences</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f4663c14-9a19-47c6-9923-df0096dd693c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3989ff5d-f737-414b-8283-6a9df21d3ce6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-4735 - Account_Registration_For_An_Already_Registered_Email</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>337f699c-3aa5-4ed8-959d-6b925bb82143</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>95734556-7757-4d98-b0e7-4856fb57f612</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-7626 - New_Unregistered_User_Creates_Account_Opted_Out</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>15defd6e-7153-4e68-9cf4-c8e64355f87d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cc79dd8b-e316-47db-acaa-997c211c784c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-7632 - Create_Account_Do_Not_Change_Default_Comms</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>693b4311-76e1-49c6-8c33-d70d8e15939a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-7633 - Create_Account_Opt_In_All_Comms</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2286ade6-83ff-435a-81e9-9a9bf771e5ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-4644 - Booking_Account_Not_Held</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc9957fc-ba82-4c44-b83d-e4e9dbb60b69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CenterParcs/CPDRMIN-7624 - Your_Details_In_Booking_Process</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
