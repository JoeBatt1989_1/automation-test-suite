import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import junit.framework.Assert as Assert

GlobalVariable.spaVillageValue = 'SF'

GlobalVariable.spaDateValue = 'today'

GlobalVariable.spaFlexibleDays = '0'

WebUI.openBrowser('https://uat.aquasana.co.uk/')

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('AquaSana/AquaSana - Homepage/Search/Spa Days/spaDayTab'), 0)

WebUI.click(findTestObject('AquaSana/AquaSana - Homepage/Search/magnetCloseOpen'))

sleep(250)

WebDriver driver = DriverFactory.getWebDriver()

String magnetClass = driver.findElement(By.cssSelector('.btn-magnet-close')).getAttribute('class')

Assert.assertEquals(magnetClass, 'btn-magnet-close js-close-booking-magnet is-visible-sm is-closed')

WebUI.click(findTestObject('AquaSana/AquaSana - Homepage/Search/magnetCloseOpen'))

sleep(250)

magnetClass = driver.findElement(By.cssSelector('.btn-magnet-close')).getAttribute('class')

Assert.assertEquals(magnetClass, 'btn-magnet-close js-close-booking-magnet is-visible-sm')

WebUI.scrollToElement(findTestObject('AquaSana/AquaSana - Homepage/hompageFooter'), 0)

WebUI.waitForElementVisible(findTestObject('AquaSana/AquaSana - Homepage/Search/bookingMagnet'), 0)

WebUI.scrollToElement(findTestObject('AquaSana/AquaSana - Homepage/aquaSanaLogo'), 0)

WebUI.waitForElementVisible(findTestObject('AquaSana/AquaSana - Homepage/Search/bookingMagnet'), 0)

WebUI.callTestCase(findTestCase('helperMethods/AquaSana/AquaSana_Search_SpaDays'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.back()

WebUI.callTestCase(findTestCase('helperMethods/AquaSana/AquaSana_Check_Magnet_Details'), [:], FailureHandling.STOP_ON_FAILURE)

GlobalVariable.spaVillageValue = 'LF'

GlobalVariable.spaDateValue = 'not today'

GlobalVariable.spaFlexibleDays = '1'

WebUI.callTestCase(findTestCase('helperMethods/AquaSana/AquaSana_Search_SpaDays'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.back()

WebUI.callTestCase(findTestCase('helperMethods/AquaSana/AquaSana_Check_Magnet_Details'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('helperMethods/AquaSana/AquaSana_Search_LuxuryBreak'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.back()

WebUI.waitForElementVisible(findTestObject('AquaSana/AquaSana - Booking Search/Spa Days/spaDayTab'), 0)

WebUI.click(findTestObject('AquaSana/AquaSana - Booking Search/Already Booked/alreadyBookedTab'))

WebUI.click(findTestObject('AquaSana/AquaSana - Booking Search/Already Booked/viewCenterParcsBtn'))

WebUI.switchToWindowIndex(1)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/emailAddress'), 0)

