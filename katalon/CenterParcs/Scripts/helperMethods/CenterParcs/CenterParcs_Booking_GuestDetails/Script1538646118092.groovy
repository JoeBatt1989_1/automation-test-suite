import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('helperMethods/Generic/SettingGuestDetails'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/firstName'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/titleDropDown'), 0)

WebUI.selectOptionByValue(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/titleDropDown'), 'OTHR', 
    true)

WebUI.setText(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/firstName'), GlobalVariable.guestFirstName)

WebUI.setText(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/lastName'), GlobalVariable.guestLastName)

WebUI.setText(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/email'), ('cpsit.tester1+' + GlobalVariable.guestEmailAddress) + 
    '@gmail.com')

WebUI.waitForElementClickable(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/guestDetailsNextBtn'), 
    0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/guestDetailsNextBtn'))

WebUI.waitForElementClickable(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/carDropDown'), 0)

WebUI.selectOptionByValue(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/carDropDown'), '0', true)

WebUI.waitForElementClickable(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/continueBtn'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Booking Journey(1)/Guest Details/continueBtn'), FailureHandling.STOP_ON_FAILURE)

