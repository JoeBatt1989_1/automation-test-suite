import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/myBookingMenu'))

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/addBookingToAccount'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - add your bookings/addBookingsTitle'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/Centerparcs - add your bookings/bookingRefInput'), 0)

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - add your bookings/bookingRefInput'), '1111')

WebUI.click(findTestObject('CenterParcs/Centerparcs - add your bookings/addBookingBtn'))

WebDriver driver = DriverFactory.getWebDriver()

WebDriverWait wait = new WebDriverWait(driver, 5)

wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath('//*[contains(text(),\'Invalid Booking Reference. No data exists for it.\')]'))))

WebUI.scrollToElement(findTestObject('CenterParcs/Centerparcs - add your bookings/bookingRefInput'), 0)

WebUI.clearText(findTestObject('CenterParcs/Centerparcs - add your bookings/bookingRefInput'), FailureHandling.STOP_ON_FAILURE)

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - add your bookings/bookingRefInput'), GlobalVariable.bookingRef)

WebUI.click(findTestObject('CenterParcs/Centerparcs - add your bookings/addBookingBtn'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/myBookingOverviewHeader'), 
    5)

