import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/payNowLink'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/payNowLink'))

WebDriver driver = DriverFactory.getWebDriver()

WebDriverWait wait = new WebDriverWait(driver, 10)

driver.switchTo().frame(0)

wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("fieldset.radio-switch-group")))

if (GlobalVariable.paySetAmount == true) {
    radioButton = driver.findElements(By.xpath("//*[@name = 'paymentOption']"))
	
	radioButton[1].click()
	
    WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/CenterParcs - Pay Outstanding Balance/enterAmount'), 
        0)

    WebUI.sendKeys(findTestObject(null), '10.00')
}

