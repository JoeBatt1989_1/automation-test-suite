import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper as JsonSlurper

WebUI.callTestCase(findTestCase('helperMethods/Generic/SettingUserDetails'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('helperMethods/Generic/SettingGuestDetails'), [:], FailureHandling.STOP_ON_FAILURE)

response = WS.sendRequest(findTestObject('WebServices/Guests API/postGuestsAddNoComms', [('emailAddress') : GlobalVariable.emailAddress]))

WS.verifyResponseStatusCode(response, 201)

JsonSlurper slurper = new JsonSlurper()

Map parsedJson = slurper.parseText(response.responseBodyContent)

GlobalVariable.uid = parsedJson.get('uid')

response2 = WS.sendRequest(findTestObject('WebServices/Sub Account API/postSubAccount', [('uid') : GlobalVariable.uid, ('guestUid') : GlobalVariable.guestUid
            , ('guestEmail') : GlobalVariable.guestEmailAddress]))

WS.verifyResponseStatusCode(response2, 200)

println(GlobalVariable.emailAddress)

