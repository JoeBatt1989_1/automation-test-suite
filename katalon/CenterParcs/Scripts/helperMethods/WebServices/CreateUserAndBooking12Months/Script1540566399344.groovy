import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.time.DayOfWeek as DayOfWeek
import java.time.LocalDate as LocalDate
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.time.temporal.TemporalAdjusters as TemporalAdjusters
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

GlobalVariable.adultsNo = '1'

GlobalVariable.bookingDates = '12 Months'

WebUI.callTestCase(findTestCase('helperMethods/WebServices/SetBookingDates'), [:], FailureHandling.STOP_ON_FAILURE)

GlobalVariable.villageCode = 'LF'

WebUI.callTestCase(findTestCase('helperMethods/WebServices/Guest/postAddGuestNoCommsPref'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('helperMethods/WebServices/Accommodation/BookAccommodation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser(GlobalVariable.paymentGatewayURL)

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Booking_Payment_Deposit - Webservices'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('helperMethods/WebServices/Accommodation/AccommodationSteps/postAccommodationConfirmation'), 
    [:], FailureHandling.STOP_ON_FAILURE)

println (GlobalVariable.emailAddress)
println (GlobalVariable.bookingRef)