import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAdjusters

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

LocalDate monday

if(GlobalVariable.bookingDates == '2 Weeks'){
	monday = LocalDate.now().plusWeeks(2)
} else if(GlobalVariable.bookingDates == '3 Months'){
	monday = LocalDate.now().plusMonths(3)
} else if (GlobalVariable.bookingDates == '6 Months'){
	monday = LocalDate.now().plusMonths(6)
} else if(GlobalVariable.bookingDates == '12 Months'){
	monday = LocalDate.now().plusMonths(12)
} else{
	monday = LocalDate.now().plusWeeks(2)
}

monday = monday.with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY))

LocalDate friday = monday

friday = friday.with(TemporalAdjusters.nextOrSame(DayOfWeek.FRIDAY))

mondayString = monday.format(formatter)

fridayString = friday.format(formatter)

GlobalVariable.arrivalDate = mondayString

GlobalVariable.departureDate = fridayString