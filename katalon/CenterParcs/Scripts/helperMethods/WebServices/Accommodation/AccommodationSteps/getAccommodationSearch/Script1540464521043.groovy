import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.List as List
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import groovy.json.JsonSlurper as JsonSlurper
import groovy.json.internal.JsonParserCharArray as JsonParserCharArray
import internal.GlobalVariable as GlobalVariable

response = WS.sendRequest(findTestObject('WebServices/Accommodation API/getAccommodationSearch', [('sessionId') : GlobalVariable.sessionId
			, ('adultsNo') : GlobalVariable.adultsNo, ('arrivalDate') : GlobalVariable.arrivalDate, ('departureDate') : GlobalVariable.departureDate
			, ('villageCode') : GlobalVariable.villageCode]))

println (response.responseBodyContent)

WS.verifyResponseStatusCode(response, 200)

JsonSlurper slurper = new JsonSlurper()

ArrayList parsedJson = slurper.parseText(response.responseBodyContent.toString())

int accomodationsList

for (accomodationsList = 0; accomodationsList < 4; accomodationsList++) {
	availableRooms = parsedJson[0].accommodations[accomodationsList].availabilities.availableRooms

	if (availableRooms != '0') {
		break
	}
}

GlobalVariable.accommodationCode = parsedJson[0].accommodations[accomodationsList].accommodationCode

GlobalVariable.villaType = parsedJson[0].accommodations[accomodationsList].accommodationCode

GlobalVariable.villaCategory = parsedJson[0].accommodations[accomodationsList].accommodationCategory