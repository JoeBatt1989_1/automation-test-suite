import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import junit.framework.Assert

WebUI.waitForElementVisible(findTestObject('AquaSana/AquaSana - Homepage/Search/Spa Days/spaDayTab'), 0)

WebDriver driver = DriverFactory.getWebDriver()

spaValue = driver.findElement(By.id("spaVillageCode")).getAttribute("value")
dateValue = driver.findElement(By.id("spaDayDatePicker")).getAttribute("value")
flexibleValue = driver.findElement(By.id("dayDifference")).getAttribute("value")

Assert.assertEquals(GlobalVariable.spaVillageValue, spaValue) 
Assert.assertEquals(GlobalVariable.dateValue, dateValue)
Assert.assertEquals(GlobalVariable.spaFlexibleDays, flexibleValue)
