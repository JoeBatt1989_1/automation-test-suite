import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.List as List
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import junit.framework.Assert as Assert

WebUI.callTestCase(findTestCase('helperMethods/WebServices/CreateUserSubAccountAndBookingWithGuest'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('https://uat.centerparcs.co.uk')

WebUI.maximizeWindow()

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/Find_out_how_it_works'), 0)

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Navigate_To_All_Bookings'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - All Bookings/allMyBreaksSubHeading'), 0)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview - Joe'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview - Joe'), 
    FailureHandling.STOP_ON_FAILURE)

WebDriver driver = DriverFactory.getWebDriver()

WebElement guestDetails = driver.findElement(By.cssSelector('.js-icon-card-lead-booker:nth-child(1)'))

WebDriverWait wait = new WebDriverWait(driver, 10)

wait.until(ExpectedConditions.visibilityOf(guestDetails))

guestDetails.click()

wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.icon-card.manual-guest")))

WebElement guestProfileCard = driver.findElement(By.cssSelector('div.icon-card.manual-guest'))

guestProfileCard.click()

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Guest Details/changeGuestDropDown - Joe'), 0)

WebUI.selectOptionByValue(findTestObject('CenterParcs/CenterParcs - Guest Details/changeGuestDropDown - Joe'), '1', false)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - Guest Details/removeGuestBtn'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Guest Details/removeGuestBtn'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Guest Details/Remove Guest Modal/removeGuestModal'), 
    0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Guest Details/Remove Guest Modal/cancelBtn'))

changeGuestDropDown = driver.findElement(By.xpath("//*[@name = 'new-guest']")).getAttribute("value")

Assert.assertEquals("1", changeGuestDropDown)
