import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('helperMethods/WebServices/CreateUserAndBooking3Months'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.openBrowser('https://uat.centerparcs.co.uk')

WebUI.maximizeWindow()

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/Find_out_how_it_works'), 0)

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Navigate_To_All_Bookings'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - All Bookings/allMyBreaksSubHeading'), 0)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview - Joe'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview - Joe'), 
    FailureHandling.STOP_ON_FAILURE)

GlobalVariable.paySetAmount = true

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/payNowLink'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/payNowLink'))

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Booking_Payment'), [:], FailureHandling.STOP_ON_FAILURE)

