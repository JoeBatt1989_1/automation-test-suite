import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

GlobalVariable.currentPassword = 'Password123'

WebUI.openBrowser('https://uat.centerparcs.co.uk')

WebUI.maximizeWindow()

WebUI.click(findTestObject('CenterParcs/Centerparcs - homepage/acceptCookieBtn'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - homepage/Header Links/Discover Center Parcs/DiscoverCPTab'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs - homepage/Header Links/Discover Center Parcs/DiscoverCPTab'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - homepage/Header Links/Discover Center Parcs/activitiesLink'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs - homepage/Header Links/Discover Center Parcs/activitiesLink'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Activities/actvitiesText'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Activities/bookActivitiesBtn'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - Things To Do/thingsToDoHeader'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/Centerparcs - Things To Do/aerialAdventureDetailsBtn'), 0)

WebUI.click(findTestObject('CenterParcs/Centerparcs - Things To Do/aerialAdventureDetailsBtn'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Activities/Aerial Adventure/aerialAdventureHeader'), 
    0)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - Activities/Aerial Adventure/signInBtn'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Activities/Aerial Adventure/signInBtn'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/submitBtn'), 0)

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/emailAddress'), 'cpsit.tester3+cpdrmin7697@gmail.com')

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/password'), GlobalVariable.currentPassword)

WebUI.click(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/submitBtn'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Activities/Aerial Adventure/aerialAdventureHeader'), 
    0)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - Activities/Aerial Adventure/activityDuration'), 0)

sleep (2000)



