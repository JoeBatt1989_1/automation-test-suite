import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

/*******************************************************************************************************
 * DESCRIPTION: This will run the process of creating a CP booking
 * 				that can be claimed by an existing account. When the user claims
 * 				a booking they will enter an incorrect booking reference before
 * 				the correct booking reference.
 *
 *      AUTHOR: Lance Edkins
 *        DATE:
 *******************************************************************************************************/
GlobalVariable.currentPassword = 'Password123'

WebUI.openBrowser('https://uat.centerparcs.co.uk')

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - homepage/signInRegisterBtn'), 0)

WebUI.click(findTestObject('CenterParcs/Centerparcs - homepage/signInRegisterBtn'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/submitBtn'), 0)

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/emailAddress'), 'cpsit.tester1+winston@gmail.com')

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/password'), GlobalVariable.currentPassword)

WebUI.click(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/submitBtn'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/Find_out_how_it_works'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/myBookingMenu'))

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/addBookingToAccount'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - add your bookings/addBookingsTitle'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/Centerparcs - add your bookings/bookingRefInput'), 0)

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - add your bookings/bookingRefInput'), '1769735')

WebUI.click(findTestObject('CenterParcs/Centerparcs - add your bookings/addBookingBtn'))

WebUI.waitForElementPresent(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/myBookingOverviewMessage'), 
    5)

