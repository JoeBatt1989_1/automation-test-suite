import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

GlobalVariable.currentPassword = 'Password123'

WebUI.openBrowser('https://uat.centerparcs.co.uk')

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - homepage/signInRegisterBtn'), 0)

WebUI.click(findTestObject('CenterParcs/Centerparcs - homepage/signInRegisterBtn'), FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Login_Existing_User'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/Find_out_how_it_works'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/myBookingMenu'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/viewAllCenterParcsBookings'), 0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/viewAllCenterParcsBookings'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - All Bookings/allMyBreaksSubHeading'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview(Specific to test CPDRMIN-4652)'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview(Specific to test CPDRMIN-4652)'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/myBookingOverviewHeader - Joe'), 
    10)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/myBookingMenu'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/payBalanceNow'), 
    5)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/payBalanceNow'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/payOutstandingBalanceMessage'), 
    5)

WebUI.callTestCase(findTestCase('helperMethods/CenterParcs/CenterParcs_Booking_Payment_SetAmount'), [:], FailureHandling.STOP_ON_FAILURE)

