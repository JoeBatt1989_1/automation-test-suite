import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

GlobalVariable.currentPassword = 'Password123'

WebUI.openBrowser('https://uat.centerparcs.co.uk')

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - homepage/signInRegisterBtn'), 0)

WebUI.click(findTestObject('CenterParcs/Centerparcs - homepage/signInRegisterBtn'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/submitBtn'), 0)

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/emailAddress'), 'cpsit.tester3+cpdrmin7720@gmail.com')

WebUI.sendKeys(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/password'), GlobalVariable.currentPassword)

WebUI.click(findTestObject('CenterParcs/Centerparcs - Login_Register/LogIn/submitBtn'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/Find_out_how_it_works'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/myBookingMenu'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/viewAllCenterParcsBookings'), 0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/viewAllCenterParcsBookings'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs - All Bookings/allMyBreaksSubHeading'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview(Specific to CPDRMIN-7720)'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/viewBookingOverview(Specific to CPDRMIN-7720)'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/myBookingOverviewHeader - Joe'), 
    0)

WebUI.scrollToElement(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/myItinerary'), 0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/myItinerary'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - My Itinerary/itineraryHeading'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - My Itinerary/addThingsToDo'), 0)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - My Itinerary/avatarIcon(Specific to CPDRMIN-7720)'), 
    0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - My Itinerary/avatarIcon(Specific to CPDRMIN-7720)'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Homepage_logged_in/myBookingMenu'))

WebUI.waitForElementVisible(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/bookingOverview'), 
    0)

WebUI.click(findTestObject('CenterParcs/Centerparcs_all_bookings/Centerparcs_booking_overview/bookingOverview'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Guest Details/guestDetailsCard(CPDRMIN-7720)'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - Guest Details/guestDetailsCard(CPDRMIN-7720)'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Guest Details/guestDetailsCard(CPDRMIN-7720)'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('CenterParcs/CenterParcs - Guest Details/guestProfileCard(CPDRMIN-7720)'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - Guest Details/guestProfileCard(CPDRMIN-7720)'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Guest Details/guestProfileCard(CPDRMIN-7720)'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('CenterParcs/CenterParcs - Guest Details/editGuestDetails'), 0)

WebUI.scrollToElement(findTestObject('CenterParcs/CenterParcs - Guest Details/removeGuestBtn'), 0)

WebUI.click(findTestObject('CenterParcs/CenterParcs - Guest Details/removeGuestBtn'), FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('CenterParcs/CenterParcs - Guest Details/removeGuestMessage'), 0)

sleep(2000)

